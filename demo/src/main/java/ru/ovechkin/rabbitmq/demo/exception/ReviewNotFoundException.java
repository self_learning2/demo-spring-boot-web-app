package ru.ovechkin.rabbitmq.demo.exception;

public class ReviewNotFoundException extends Exception {

    public ReviewNotFoundException() {
        super("Review not found");
    }

}
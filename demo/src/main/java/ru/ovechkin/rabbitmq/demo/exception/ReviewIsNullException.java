package ru.ovechkin.rabbitmq.demo.exception;

public class ReviewIsNullException extends Exception {

    public ReviewIsNullException() {
        super("Review is null");
    }

}
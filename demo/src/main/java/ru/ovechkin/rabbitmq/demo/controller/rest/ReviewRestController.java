package ru.ovechkin.rabbitmq.demo.controller.rest;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.rabbitmq.demo.api.srervice.IReviewService;
import ru.ovechkin.rabbitmq.demo.consts.Constant;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;

import java.util.List;

@RestController
@RequestMapping("/api/review")
public class ReviewRestController {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private IReviewService reviewService;

    @Value("${spring.rabbitmq.template.exchange}")
    private String topic;

    @PostMapping("/create")
    public String create(
            @RequestBody final ReviewDTO reviewDTO
    ) {
        template.convertAndSend(topic, Constant.CREATE_KEY, reviewDTO);
        return "Success! Review is being added...";
    }

    @DeleteMapping("/remove/{id}")
    public String remove(
            @PathVariable("id") final String reviewId
    ) {
        template.convertAndSend(topic, Constant.DELETE_KEY, reviewId);
        return "Success! Review is being deleted...";
    }

    @GetMapping("/main")
    public List<ReviewDTO> findAll() {
        return reviewService.findAll();
    }

    @PutMapping("/edit")
    public List<ReviewDTO> edit(
            @RequestBody final ReviewDTO reviewDTO
    ) {
        return template.convertSendAndReceiveAsType(
                topic,
                Constant.UPDATE_KEY,
                reviewDTO,
                new ParameterizedTypeReference<List<ReviewDTO>>(){});
    }

}
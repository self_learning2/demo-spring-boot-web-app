package ru.ovechkin.rabbitmq.demo.api.srervice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.entity.Review;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewIsNullException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewNotFoundException;

import java.util.List;

public interface IReviewService {

    void create(@Nullable ReviewDTO reviewDTO) throws ReviewIsNullException;

    @NotNull ReviewDTO findById(@Nullable String id) throws IdEmptyException, ReviewNotFoundException;

    List<ReviewDTO> findAll();

    void updateById(
            @Nullable String id,
            @Nullable ReviewDTO reviewDTO
    ) throws ReviewNotFoundException, IdEmptyException, ReviewIsNullException;

    void deleteById(@Nullable String id) throws IdEmptyException;
}

package ru.ovechkin.rabbitmq.demo.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.ovechkin.rabbitmq.demo.api.repository.ReviewRepository;
import ru.ovechkin.rabbitmq.demo.api.srervice.IReviewService;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.entity.Review;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReviewService implements IReviewService {

    @Setter
    private ReviewRepository reviewRepository;

    @Override
    public void create(@Nullable final ReviewDTO reviewDTO) {
        Optional.ofNullable(reviewDTO)
                .map(ReviewDTO::toEntity)
                .ifPresent(reviewRepository::save);
    }

    @NotNull
    @Override
    public ReviewDTO findById(@Nullable final String id) throws ReviewNotFoundException {
        return Optional.ofNullable(id)
                .flatMap(reviewRepository::findById)
                .map(Review::toDTO)
                .orElseThrow(ReviewNotFoundException::new);
    }

    @Override
    public List<ReviewDTO> findAll() {
        return reviewRepository.findAll().stream()
                .map(Review::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void updateById(@Nullable final String id, @Nullable final ReviewDTO reviewDTO)
            throws IdEmptyException {

        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Optional.ofNullable(reviewDTO)
                .map(ReviewDTO::toEntity)
                .map(review -> {
                    review.setId(id);
                    return review;
                })
                .ifPresent(reviewRepository::save);
    }

    @Override
    public void deleteById(@Nullable final String id) {
        Optional.ofNullable(id)
                .ifPresent(reviewRepository::deleteById);
    }

}